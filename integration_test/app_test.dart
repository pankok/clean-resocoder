import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:integration_test/integration_test.dart';
import 'package:number_trivial_app/features/number_trivia/presentation/widgets/trivia_display_widget.dart';
import 'package:number_trivial_app/main.dart';

void main() {
  IntegrationTestWidgetsFlutterBinding.ensureInitialized();

  testWidgets(
    "Digitando numero e mostrado dado sobre o mesmo",
    (WidgetTester tester) async {
      await tester.pumpWidget(MyApp());

      final inputText = '12';
      await tester.enterText(find.byKey(Key('number-text')), inputText);

      await tester.tap(find.byKey(Key('get-by-text')));
      await tester.pumpAndSettle();

      expect(find.byType(TriviaDisplayWidget), findsOneWidget);
    },
  );
}
