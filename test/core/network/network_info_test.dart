import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:number_trivial_app/core/platform/network_info.dart';

import 'network_info_test.mocks.dart';

@GenerateMocks([Connectivity])
void main() {
  late NetworkInfoImpl networkInfo;
  late MockConnectivity mockConnect;

  setUp(() {
    mockConnect = MockConnectivity();
    networkInfo = NetworkInfoImpl(mockConnect);
  });

  group('isConnected', () {
    test(
      'should forward the call to DataConnectionChecker.hasConnection',
      () async {
        final tHasConnectionFuture = Future.value(ConnectivityResult.mobile);
        when(mockConnect.checkConnectivity())
            .thenAnswer((_) => tHasConnectionFuture);
        final result = networkInfo.isConnected;
        verify(mockConnect.checkConnectivity());
        expect(await result, await tHasConnectionFuture);
      },
    );
  });
}
