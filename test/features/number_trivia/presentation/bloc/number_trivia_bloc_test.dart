import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:number_trivial_app/core/error/failure.dart';
import 'package:number_trivial_app/core/usercases/usercase.dart';
import 'package:number_trivial_app/core/util/input_convert.dart';
import 'package:number_trivial_app/features/number_trivia/domain/entities/number_trivia.dart';
import 'package:number_trivial_app/features/number_trivia/domain/usecases/get_concrete_number_trivia.dart';
import 'package:number_trivial_app/features/number_trivia/domain/usecases/get_random_number_trivia.dart';
import 'package:number_trivial_app/features/number_trivia/presentation/bloc/number_trivia_bloc.dart';

import 'number_trivia_bloc_test.mocks.dart';

@GenerateMocks([GetConcreteNumberTrivia, GetRandomNumberTrivia, InputConverter])
void main() {
  late NumberTriviaBloc bloc;
  late MockGetConcreteNumberTrivia mockGetConcreteNumberTrivia;
  late MockGetRandomNumberTrivia mockGetRandomNumberTrivia;
  late MockInputConverter mockInputConverter;

  setUp(() {
    mockGetConcreteNumberTrivia = MockGetConcreteNumberTrivia();
    mockGetRandomNumberTrivia = MockGetRandomNumberTrivia();
    mockInputConverter = MockInputConverter();

    bloc = NumberTriviaBloc(
      concrete: mockGetConcreteNumberTrivia,
      random: mockGetRandomNumberTrivia,
      inputConverter: mockInputConverter,
    );
  });
  tearDown(() {
    reset(mockGetConcreteNumberTrivia);
    reset(mockGetRandomNumberTrivia);
    reset(mockInputConverter);
  });

  test('initialState should be Empty', () {
    expect(bloc.state, Empty());
  });
  group('GetTriviaForConcreteNumber', () {
    final tNumberString = '1';
    final tNumberParsed = int.parse(tNumberString);
    final tNumberTrivia = NumberTrivia(text: 'test trivia');

    test(
      'should emit [Error] when the input is invalid',
      () async {
        when(mockInputConverter.stringToUnsignedInteger('1'))
            .thenReturn(Left(InvalidInputFailure()));
        bloc.add(GetTriviaForConcreteNumber(tNumberString));
        bloc.close();
        await expectLater(
            bloc.stream,
            emitsInOrder([
              isA<Error>(),
              emitsDone,
            ]));
      },
    );
    test(
      'should get data from the concrete use case',
      () async {
        when(mockInputConverter.stringToUnsignedInteger('1'))
            .thenReturn(Right(tNumberParsed));
        when(mockGetConcreteNumberTrivia(Params(number: 1)))
            .thenAnswer((_) async => Right(tNumberTrivia));
        bloc.add(GetTriviaForConcreteNumber(tNumberString));
        await untilCalled(mockGetConcreteNumberTrivia(Params(number: 1)));
        verify(mockGetConcreteNumberTrivia(Params(number: tNumberParsed)));
      },
    );
    test(
      'should emit [Loading, Loaded] when data is gotten successfully',
      () async {
        when(mockInputConverter.stringToUnsignedInteger('1'))
            .thenReturn(Right(tNumberParsed));
        when(mockGetConcreteNumberTrivia(any))
            .thenAnswer((_) async => Right(tNumberTrivia));
        final expected = [
          Loading(),
          Loaded(trivia: tNumberTrivia),
        ];
        expectLater(bloc.stream, emitsInOrder(expected));
        bloc.add(GetTriviaForConcreteNumber(tNumberString));
      },
    );
    test(
      'should emit [Loading, Error] when getting data fails',
      () async {
        when(mockInputConverter.stringToUnsignedInteger('1'))
            .thenReturn(Right(tNumberParsed));
        when(mockGetConcreteNumberTrivia(any))
            .thenAnswer((_) async => Left(ServerFailure()));
        bloc.add(GetTriviaForConcreteNumber(tNumberString));
        final expected = [
          Loading(),
          Error(message: SERVER_FAILURE_MESSAGE),
        ];
        expectLater(bloc.stream, emitsInOrder(expected));
      },
    );
    test(
      'should emit [Loading, Error] with a proper message for the error when getting data fails',
      () async {
        when(mockInputConverter.stringToUnsignedInteger('1'))
            .thenReturn(Right(tNumberParsed));
        when(mockGetConcreteNumberTrivia(any))
            .thenAnswer((_) async => Left(CacheFailure()));
        bloc.add(GetTriviaForConcreteNumber(tNumberString));
        final expected = [
          Loading(),
          Error(message: CACHE_FAILURE_MESSAGE),
        ];
        expectLater(bloc.stream, emitsInOrder(expected));
      },
    );
  });
  group('GetTriviaForRandomNumber', () {
    final tNumberTrivia = NumberTrivia(text: 'test trivia');
    test(
      'should get data from the random use case',
      () async {
        when(mockGetRandomNumberTrivia(any))
            .thenAnswer((_) async => Right(tNumberTrivia));
        bloc.add(GetTriviaForRandomNumber());
        await untilCalled(mockGetRandomNumberTrivia(any));
        verify(mockGetRandomNumberTrivia(NoParams()));
      },
    );
    test(
      'should emit [Loading, Loaded] when data is gotten successfully',
      () async {
        when(mockGetRandomNumberTrivia(any))
            .thenAnswer((_) async => Right(tNumberTrivia));
        bloc.add(GetTriviaForRandomNumber());
        final expected = [
          Loading(),
          Loaded(trivia: tNumberTrivia),
        ];
        expectLater(bloc.stream, emitsInOrder(expected));
      },
    );

    test(
      'should emit [Loading, Error] when getting data fails',
      () async {
        when(mockGetRandomNumberTrivia(any))
            .thenAnswer((_) async => Left(ServerFailure()));
        bloc.add(GetTriviaForRandomNumber());
        final expected = [
          Loading(),
          Error(message: SERVER_FAILURE_MESSAGE),
        ];
        expectLater(bloc.stream, emitsInOrder(expected));
      },
    );

    test(
      'should emit [Loading, Error] with a proper message for the error when getting data fails',
      () async {
        when(mockGetRandomNumberTrivia(any))
            .thenAnswer((_) async => Left(CacheFailure()));
        bloc.add(GetTriviaForRandomNumber());
        final expected = [
          Loading(),
          Error(message: CACHE_FAILURE_MESSAGE),
        ];
        expectLater(bloc.stream, emitsInOrder(expected));
      },
    );
  });
}
