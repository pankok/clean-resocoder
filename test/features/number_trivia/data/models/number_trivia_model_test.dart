import 'package:flutter_test/flutter_test.dart';
import 'package:number_trivial_app/features/number_trivia/data/models/number_trivia_model.dart';
import 'package:number_trivial_app/features/number_trivia/domain/entities/number_trivia.dart';

import '../../../../fixtures/fixture_reader.dart';

void main() {
  final tNumberTriviaModel = NumberTriviaModel(text: "Test Text");

  test(
    'should be a subclass of NumberTrivia entity',
    () async {
      expect(tNumberTriviaModel, isA<NumberTrivia>());
    },
  );
  group('fromJson', () {
    test(
      'should return a valid model when the JSON number is an integer',
      () async {
        final String dataNumber = fixture('trivia.txt');
        final result = NumberTriviaModel.fromJson(dataNumber);
        expect(result, tNumberTriviaModel);
      },
    );
    test(
      'should return a valid model when the JSON number is regarded as a double',
      () async {
        final String dataNumber = fixture('trivia_double.txt');
        final result = NumberTriviaModel.fromJson(dataNumber);
        expect(result, tNumberTriviaModel);
      },
    );
  });
  group('toJson', () {
    test(
      'should return a JSON map containing the proper data',
      () async {
        final result = tNumberTriviaModel.toJson();
        final expectedJsonMap = "Test Text";
        expect(result, expectedJsonMap);
      },
    );
  });
}
