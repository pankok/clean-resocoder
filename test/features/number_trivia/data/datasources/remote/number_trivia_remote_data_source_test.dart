import 'package:dio/dio.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:number_trivial_app/core/error/exception.dart';
import 'package:number_trivial_app/features/number_trivia/data/datasources/remota/number_trivia_remote_data_source_impl.dart';
import 'package:number_trivial_app/features/number_trivia/data/models/number_trivia_model.dart';
import '../../../../../fixtures/fixture_reader.dart';
import 'number_trivia_remote_data_source_test.mocks.dart';

class DioMock extends Mock implements Dio {}

@GenerateMocks([DioMock])
void main() {
  late NumberTriviaRemoteDataSourceImpl dataSource;
  late MockDioMock mockDio;
  final tNumberTriviaModel = NumberTriviaModel.fromJson(fixture('trivia.txt'));
  final responsePayload = fixture('trivia.txt');

  mockDio = MockDioMock();
  dataSource = NumberTriviaRemoteDataSourceImpl(client: mockDio);

  tearDown(() {
    reset(mockDio);
  });

  group('getConcreteNumberTrivia', () {
    final tNumber = 1;
    test(
      'should return NumberTrivia when the response code is 200 (success)',
      () async {
        when(mockDio.get('http://numbersapi.com/1')).thenAnswer(
          (realInvocation) async {
            return Response(
              data: responsePayload,
              statusCode: 200,
              requestOptions: RequestOptions(path: 'http://numbersapi.com/1'),
            );
          },
        );
        final result = await dataSource.getConcreteNumberTrivia(tNumber);
        expect(result, equals(tNumberTriviaModel));
      },
    );

    test(
      'should throw a ServerException when the response code is 404 or other',
      () async {
        when(mockDio.get('http://numbersapi.com/1')).thenAnswer(
          (realInvocation) async {
            return Response(
              statusCode: 404,
              requestOptions: RequestOptions(path: 'http://numbersapi.com/1'),
            );
          },
        );
        expect(() => dataSource.getConcreteNumberTrivia(tNumber),
            throwsA(isA<ServerException>()));
      },
    );
  });

  group('getRandomNumberTrivia', () {
    final tNumberTriviaModel =
        NumberTriviaModel.fromJson(fixture('trivia.txt'));
    test(
      'should return NumberTrivia when the response code is 200 (success)',
      () async {
        when(mockDio.get('http://numbersapi.com/random')).thenAnswer(
          (realInvocation) async {
            return Response(
              data: responsePayload,
              statusCode: 200,
              requestOptions:
                  RequestOptions(path: 'http://numbersapi.com/random'),
            );
          },
        );
        final result = await dataSource.getRandomNumberTrivia();
        expect(result, equals(tNumberTriviaModel));
      },
    );

    test(
      'should throw a ServerException when the response code is 404 or other',
      () async {
        when(mockDio.get('http://numbersapi.com/random')).thenAnswer(
          (realInvocation) async {
            return Response(
              statusCode: 404,
              requestOptions:
                  RequestOptions(path: 'http://numbersapi.com/random'),
            );
          },
        );
        expect(() => dataSource.getRandomNumberTrivia(),
            throwsA(isA<ServerException>()));
      },
    );
  });
}
