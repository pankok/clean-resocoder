import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:number_trivial_app/core/error/exception.dart';
import 'package:number_trivial_app/features/number_trivia/data/datasources/local/number_trivia_local_data_source_impl.dart';
import 'package:number_trivial_app/features/number_trivia/data/models/number_trivia_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../../fixtures/fixture_reader.dart';
import 'number_trivia_local_data_source_impl_test.mocks.dart';

@GenerateMocks([SharedPreferences])
void main() {
  late NumberTriviaLocalDataSourceImpl dataSource;
  late MockSharedPreferences mockSharedPreferences;

  setUp(() {
    mockSharedPreferences = MockSharedPreferences();
    dataSource = NumberTriviaLocalDataSourceImpl(
      sharedPreferences: mockSharedPreferences,
    );
  });

  group('getLastNumberTrivia', () {
    final tNumberTriviaModel =
        NumberTriviaModel.fromJson(fixture('trivia_cached.txt'));

    test(
      'should return NumberTrivia from SharedPreferences when there is one in the cache',
      () async {
        when(mockSharedPreferences.getString('CACHED_NUMBER_TRIVIA'))
            .thenReturn(fixture('trivia_cached.txt'));
        final result = await dataSource.getLastNumberTrivia();
        verify(mockSharedPreferences.getString('CACHED_NUMBER_TRIVIA'));
        expect(result, equals(tNumberTriviaModel));
      },
    );
    test('should throw a CacheException when there is not a cached value', () {
      when(mockSharedPreferences.getString('CACHED_NUMBER_TRIVIA'))
          .thenReturn(null);
      expect(() => dataSource.getLastNumberTrivia(),
          throwsA(isA<CacheException>()));
    });
  });
}
