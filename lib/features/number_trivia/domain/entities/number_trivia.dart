import 'package:equatable/equatable.dart';

class NumberTrivia extends Equatable {
  final String text;

  NumberTrivia({
    required this.text,
  });

  @override
  List<Object?> get props => [text];
}
