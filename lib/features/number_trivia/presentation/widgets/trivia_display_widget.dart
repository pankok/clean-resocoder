import 'package:flutter/material.dart';
import 'package:number_trivial_app/features/number_trivia/domain/entities/number_trivia.dart';

class TriviaDisplayWidget extends StatelessWidget {
  final NumberTrivia numberTrivia;

  const TriviaDisplayWidget({required this.numberTrivia});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height / 3,
      child: Column(
        children: <Widget>[
          Expanded(
            child: Center(
              child: SingleChildScrollView(
                child: Text(
                  numberTrivia.text,
                  style: TextStyle(fontSize: 25),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
