import '../../domain/entities/number_trivia.dart';

class NumberTriviaModel extends NumberTrivia {
  NumberTriviaModel({
    required String text,
  }) : super(text: text);

  factory NumberTriviaModel.fromJson(String json) {
    return NumberTriviaModel(
      text: json,
    );
  }

  String toJson() {
    return text;
  }
}
