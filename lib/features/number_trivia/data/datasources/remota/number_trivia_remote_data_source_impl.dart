import 'package:dio/dio.dart';
import 'package:number_trivial_app/core/error/exception.dart';
import 'package:number_trivial_app/features/number_trivia/data/models/number_trivia_model.dart';
import 'number_trivia_remote_data_source.dart';

class NumberTriviaRemoteDataSourceImpl implements NumberTriviaRemoteDataSource {
  final Dio client;

  NumberTriviaRemoteDataSourceImpl({required this.client});

  @override
  Future<NumberTriviaModel> getConcreteNumberTrivia(int number) =>
      _getTriviaFromUrl('http://numbersapi.com/$number');

  @override
  Future<NumberTriviaModel> getRandomNumberTrivia() =>
      _getTriviaFromUrl('http://numbersapi.com/random');

  Future<NumberTriviaModel> _getTriviaFromUrl(String url) async {
    try {
      print(url);
      final response = await client.get(url);
      print(response);
      return NumberTriviaModel.fromJson(response.data);
    } catch (e) {
      throw ServerException();
    }
  }
}
