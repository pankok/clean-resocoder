import 'package:number_trivial_app/core/error/exception.dart';
import 'package:number_trivial_app/features/number_trivia/data/models/number_trivia_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'number_trivia_local_data_source.dart';

const CACHED_NUMBER_TRIVIA = 'CACHED_NUMBER_TRIVIA';

class NumberTriviaLocalDataSourceImpl implements NumberTriviaLocalDataSource {
  final SharedPreferences sharedPreferences;

  NumberTriviaLocalDataSourceImpl({required this.sharedPreferences});

  @override
  cacheNumberTrivia(NumberTriviaModel triviaToCache) {
    return sharedPreferences.setString(
      CACHED_NUMBER_TRIVIA,
      triviaToCache.toJson(),
    );
  }

  @override
  Future<NumberTriviaModel> getLastNumberTrivia() {
    try {
      final jsonString = sharedPreferences.getString(CACHED_NUMBER_TRIVIA);
      return Future.value(NumberTriviaModel.fromJson(jsonString!));
    } catch (e) {
      return throw CacheException();
    }
  }
}
